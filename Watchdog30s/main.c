/*  Exemplo de watchdog com timeout superior a 2 segundos

    Este programa basicamente usa o watchdog como interrup��o para emular um contador
    independente que incrementa uma variavel global e quando esta chega a um determinado
	valor (timeout), o watchdog muda para o modo reset e reseta o MCU e, caso um dos bot�es
	seja pressionado, o watchdog � reiniciado.
	
	Neste exemplo, assumo que haver�o bot�es ligados � porta B (PORTB) e LEDs ligados �
	porta C (PORTC).
	
	Os LEDs ser�o utilizados da seguinte maneira:
	
	7  |  6  |  5  |  4  |  3  |  2  |  1  |  0
	1  |  1  |  0  |  0  |  x  |  x  |  x  |  x  = Indica que o MCU n�o foi resetado
	0  |  0  |  1  |  1  |  x  |  x  |  x  |  x  = Indica que o MCU foi resetado
    x  |  x  |  x  |  x  |  a  |  b  |  c  |  d  = Mostra o valor binario do contador
	
	------------------------------------------------------------------------------
	
    Registrador WDTCSR 

      7  |   6  |   5  |   4  |   3  |   2  |   1  |   0
    WDIF | WDIE | WDP3 | WDCE | WDE  | WDP2 | WDP1 | WDP0

    Bit 7 - WDIF: Watchdog Interrupt Flag (setado automaticamente pelo MCU)
	Bit 6 - WDIE: Watchdog Interrupt Enable (habilita interrup��o ao inv�s de reset)
	
	B7 | B6 | Mode
	---|----|----------------------
	 0 |  0 | Stopped
	 0 |  1 | Interrupt (WDT_vect)
	 1 |  0 | Reset
	 1 |  1 | Interrupt + Reset

     Bit 4 - WDCE: Watchdog Change Enable (usado em conjunto com WDP3:0)
	 Bit 3 - WDE: Watchdog System Reset Enable (Habilita o watchdog)
	 Bit 5, 2:0 - WDP3:0: Watchdog Timer Prescaler 3, 2, 1 and 0
	 
	 WDP3 | WDP2 | WDP1 | WDP0 | Ciclos          | Timeout
	   0  |   0  |   0  |   0  | 2K (2048)       |   16ms
	   0  |   0  |   0  |   1  | 4K (4096)       |   32ms
	   0  |   0  |   1  |   0  | 8K (8192)       |   64ms
	   0  |   0  |   1  |   1  | 16K (16384)     |   125ms
	   0  |   1  |   0  |   0  | 32K (32768)     |   250ms
	   0  |   1  |   0  |   1  | 64K (65536)     |   500ms
	   0  |   1  |   1  |   0  | 128K (131072)   |   1s
	   0  |   1  |   1  |   1  | 256K (262144)   |   2s
	   1  |   0  |   0  |   0  | 512K (524288)   |   4s
	   1  |   0  |   0  |   1  | 1024K (1048576) |   8s
	   1  |   x  |   x  |   x  |    Reservado    |   Reservado
	   
	   ATEN��O: N�O tente setar todos os bits necess�rios de uma vez s�, 
	            tampouco tente setar os bits usando WDTCSR |=, use WDTCSR =
	   
*/

//Define as macros de I/O (i.e. DDRx,PORTx,etc)
#include <avr/io.h>

//Define as macros de Interrupt (ISR)
#include <avr/interrupt.h>

//Define a velocidade do MCU (16MHz) para a fun��o _delay_ms()                     
#define F_CPU 16000000UL                       
//Importa a fun��o _delay_ms()
#include <util/delay.h>                        

//Macro para resetar o Watchdog
#define WDR __asm__ __volatile__ ("wdr")
//Timeout (em segs) para o Watchdog
#define WD_TO   15

#define WD_MODE_INT   0   //Define o modo interrupt                         
#define WD_MODE_RESET 1   //Define o modo reset
   
//Contador auxiliar para o WDTO   
static unsigned char wdt_counter = 0;                  

// Fun��o para configurar e iniciar o watchdog
// Caso o parametro passado seja WD_MODE_RESET, define o watchdog para o modo RESET
// caso contrario, define o watchdog para o modo INTERRUPT
void initWatchdog(unsigned char isReset) {                   
	if (MCUSR & (1<<WDRF)) {  //Caso tenhamos sofrido reset
	  MCUSR &= ~(1<<WDRF);	  //Limpa a flag de reset
	}
	
	WDR;        //Garante que o WD foi resetado
	cli();      //Desabilita interrup�oes
	WDTCSR = 0; //Limpa o registrador WDTCSR
	             
	// ATEN�AO: O modo de configura��o (WDCE) deve ser chamado imediatamente
	// antes da instru��o que define o prescaler (WDPx), por isso essa instru��o
	// se repete dentro da estrutura condicional abaixo
	
	if(isReset){  // Modo RESET
	  WDTCSR = (1<<WDE) | (1<<WDCE);  //Entra em modo de configura��o
	  WDTCSR = (1<<WDE);              //Modo RESET e timeout em 16ms	
	} else {      // Modo Interrupt
	  WDTCSR = (1<<WDE) | (1<<WDCE);               //Entra em modo de configura��o
	  WDTCSR = (1<<WDIE) | (1<<WDP2) | (1<<WDP1);  //Modo Interrup�ao e timeout em 1s
	}
	
	sei();  //Reabilita as interrupcoes
}


ISR(WDT_vect) {
	if (++wdt_counter > WD_TO) {     // Se chegar nos 30s...
		initWatchdog(WD_MODE_RESET);   // Reconfigura o WD para o modo RESET
		wdt_counter = 0;
	}

    unsigned char p = PINC & 0b11110000;
	p |= 0x0F & ~wdt_counter;
    PORTC = p;
	
}

// Fun��o para ajuste inicial de portas
void setup(void) {
   DDRB = 0x00;  //Port B, todos os pinos como INPUT
   DDRC = 0xff;  //Port C, todos os pinos como OUTPUT
   
   
   if (MCUSR & (1<<WDRF)){   // Caso o MCU tenha sido resetado...
		PORTC = 0b11001111;  // Liga os LEDs de 5:4
   } else {                  // Sen�o..
		PORTC = 0b00111111;  // Liga os LEDs de 7:6   
   }
}

int main(void)
{
	setup();                    // Setup de portas  
	initWatchdog(WD_MODE_INT);  // Setup do Watchdog
	
	// Este delay emula uma condi��o onde o codigo fica travado,
	// impedindo que outras partes importantes rodem...
	//_delay_ms(1000*(WD_TO+3));
	
    while (1) // Loop infinito e comentario inutil :D
    {
		if ((PINB & (1<<7)) == 0) { //Caso o botao 7 seja pressionado 
			WDR;              //reseta o watchdog
			wdt_counter = 0;  //reinicia o contador de TO extendido
		}
    }
	
}

